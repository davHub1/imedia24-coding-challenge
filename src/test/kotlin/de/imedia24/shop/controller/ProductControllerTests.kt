package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import de.imedia24.shop.db.entity.UpdatableProductEntity
import org.hamcrest.Matchers.containsInAnyOrder
import org.hamcrest.Matchers.`is`
import org.springframework.http.MediaType
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.math.BigDecimal

@SpringBootTest
@AutoConfigureMockMvc
internal class ProductControllerTests {
    @Autowired
    lateinit var mockMvc: MockMvc

    @Test
    fun `Assert fetch multiple products`() {
        val skus: String = "101,102,103"
        mockMvc.perform(MockMvcRequestBuilders.get("/products?skus=$skus"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$[*].sku", containsInAnyOrder("101", "102", "103")))
    }

    @Test
    fun `Assert update product name, description and price`() {
        val mapper = ObjectMapper().registerModule(KotlinModule())

        val updatedInstance = UpdatableProductEntity()
        updatedInstance.name = "Updatable Name"
        updatedInstance.description = "Updatable Description"
        updatedInstance.price = BigDecimal(666)

        mockMvc.perform(
            MockMvcRequestBuilders.put("/products/103")
                .contentType(
                    MediaType.APPLICATION_JSON
                )
                .content(mapper.writeValueAsString(updatedInstance))
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andExpect(MockMvcResultMatchers.jsonPath("$.name", `is`(updatedInstance.name)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.description", `is`(updatedInstance.description)))
            .andExpect(MockMvcResultMatchers.jsonPath("$.price", `is`(updatedInstance.price.toInt())))

    }
}

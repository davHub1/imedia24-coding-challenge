package de.imedia24.shop.db.entity

import java.math.BigDecimal

data class UpdatableProductEntity(
    var name: String = "",
    var description: String? = null,
    var price: BigDecimal = BigDecimal(0)
)
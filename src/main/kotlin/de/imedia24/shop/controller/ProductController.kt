package de.imedia24.shop.controller

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.UpdatableProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.websocket.server.PathParam

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @RequestParam skus: List<String>
    ): ResponseEntity<List<ProductResponse?>> {
        logger.info("Request for products $skus")

        val products = productService.findProductsBySkus(skus)
        return if(products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun saveProduct(
        @RequestBody product: ProductEntity
    ): ResponseEntity<ProductResponse> {
        logger.info("Saving product $product")

        val prod = productService.saveProduct(product)
        return if(prod == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(prod)
        }
    }

    @PutMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody product: UpdatableProductEntity
    ): ResponseEntity<ProductResponse> {
        logger.info("Updating product $product with sku $sku")

        val prod = productService.updateProduct(sku,product)
        return if(prod == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(prod)
        }
    }
}

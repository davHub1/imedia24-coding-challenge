package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.entity.UpdatableProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductsBySkus(skus: List<String>): List<ProductResponse?> {

        val products = skus.map{
            productRepository.findBySku(it)?.toProductResponse()
        }
        return products
    }

    fun saveProduct(product: ProductEntity): ProductResponse? {
        return productRepository.save(product)?.toProductResponse()
    }

    fun updateProduct(sku: String, product: UpdatableProductEntity): ProductResponse? {
        val prod: ProductEntity? = productRepository.findBySku(sku)
        prod!!.name = product.name
        prod!!.description = product.description
        prod!!.price = product.price

        return productRepository.save(prod)?.toProductResponse()
    }
}

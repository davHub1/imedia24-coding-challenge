ALTER TABLE products ADD qty int NOT NULL;
ALTER TABLE products ADD location VARCHAR (255) NOT NULL;

insert into products values ('101','Samsung Smart TV','A  smart TV with android from Samsung',12000,parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),10, 'Somewhere');
insert into products values ('102','Apple Watch','A smart watch by Apple',1200,parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),120, 'Everywhere');
insert into products values ('103','BMW M3','High Performance Car by BMW',45000000,parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'),parsedatetime('17-09-2012 18:47:52.69', 'dd-MM-yyyy hh:mm:ss.SS'), 3, 'Probably germany');